## Usage

1. Convert to LMDB format

    ```
    $ python convert_to_lmdb.py --data_dir ./data
    ```

2. Train

    ```
    $ python train.py --data_dir ./data --logdir ./logs
    ```

3. Retrain if you need

    ```
    $ python train.py --data_dir ./data --logdir ./logs_retrain --restore_checkpoint ./logs/model-100.pth
    ```

4. Evaluate

    ```
    $ python eval.py --data_dir ./data ./logs/model-100.pth
    ```

5. Infer

    ```
    $ python infer.py --checkpoint=./logs/model-100.pth ./images/test1.png
    ```

6. Clean

    ```
    $ rm -rf ./logs
    or
    $ rm -rf ./logs_retrain
    ```