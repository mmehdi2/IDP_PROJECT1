import argparse
import glob
import os
import random
import h5py
import lmdb
import numpy as np
from PIL import Image
import example_pb2
from meta import Meta

parser = argparse.ArgumentParser()
parser.add_argument('-d','--data-dir', default='./data', help='directory to SVHN (format 1) folders and write the converted files')

class Reader(object):
    def __init__(self,path):
        self._path = path
        self._num = len(self._path)
        self._example = 0
    
    @staticmethod
    def _get_attrs(digit_mat_file, index):
        """return dict with keys: label,left,top,width and height"""
        attrs = {}
        f = digit_mat_file
        item = f['digitStruct']['bbox'][index].item()
        for key in ['label','left','top','width','height']:
            attr = f[item][key]
            values = [f[attr[i].item()][0][0]
                     for i in range(len(attr))] if len(attr)>1 else [attr[0][0]]
            attrs[key] = values
        return attrs
    
    @staticmethod
    def _preprocess(image,box_left,box_top,box_width,box_height):
        cropL,cropT,cropW,cropH = (int(round(box_left - 0.15 * box_width)),
                                   int(round(box_top - 0.15 * box_height)),
                                   int(round(box_width*1.3)),
                                   int(round(box_height*1.3)))
        image = image.crop([cropL,cropT,cropL+cropW,cropT+cropH])
        image = image.resize([64,64])
        return image
    
    def read_convert(self,digit_mat_file):
        if self._example == self._num:
            return None
        path = self._path[self._example]
        index = int(path.split('\\')[-1].split('.')[0])-1
        self._example += 1

        attrs = Reader._get_attrs(digit_mat_file,index)
        label_digits = attrs['label']
        length = len(label_digits)
        if length > 5:
            return self.read_convert(digit_mat_file)
        digits = [10,10,10,10,10] #10 means no digits
        for idx, label_digits in enumerate(label_digits):
            digits[idx] = int(label_digits if label_digits != 10 else 0)
        
        attrsL,attrsT,attrsW,attrsH = map(lambda x: [int(i) for i in x],[attrs['left'],attrs['top'], attrs['width'], attrs['height']])
        minL,minT,maxR,maxB = (min(attrsL),
                               min(attrsT),
                               max(map(lambda x, y: x + y, attrsL, attrsW)),
                               max(map(lambda x, y: x + y, attrsT, attrsH)))
        centx,centy,maxs = ((minL+maxR)/2.0,
                            (minT+maxB)/2.0,
                            max(maxR-minL,maxB-minT))
        boxL, boxT, boxW, boxH = (centx - maxs / 2.0,
                                  centy - maxs / 2.0,
                                  maxs,
                                  maxs)
        image = np.array(Reader._preprocess(Image.open(path),boxL,boxT,boxW,boxH)).tobytes()
        
        example = example_pb2.Example()
        example.image = image
        example.length = length
        example.digits.extend(digits)
        return example

def convertlmdb(path_dataset_dir, path_lmdb_dirs, choose_writer_callback):
    num_example = []
    writer = []
    
    for path_lmdb_dir in path_lmdb_dirs:
        num_example.append(0)
        writer.append(lmdb.open(path_lmdb_dir, map_size = 10*1024*1024*1024))
    
    for path_dataset, path_matfile in path_dataset_dir:
        path_image_files = glob.glob(os.path.join(path_dataset, '*.png'))
        total_files = len(path_image_files)
        print('%d files found in %s' %(total_files,path_dataset))
        
        with h5py.File(path_matfile, 'r') as digit_mat_file:
            example_reader = Reader(path_image_files)
            block_size = 10000
            
            for i in range(0, total_files, block_size):
                txns = [writers.begin(write=True) for writers in writer ]
                
                for ofset in range(block_size):
                    idx = choose_writer_callback(path_lmdb_dirs)
                    txn = txns[idx]
                    
                    example = example_reader.read_convert(digit_mat_file)
                    if example is None:
                        break
                    
                    str_id = '{:08}'.format(num_example[idx]+1)
                    txn.put(str_id.encode(),example.SerializeToString())
                    num_example[idx] += 1
                    index = i+ofset
                    path_image_file = path_image_files[index]
                    print('(%d/%d) %s'%(index+1,total_files,path_image_file))
                [txn.commit() for txn in txns]
    for writers in writer:
        writers.close()
    return num_example

def create_lmdb_meta_file(num_train_examples, num_val_exam, num_test_examples, path_to_lmdb_meta_file):
    print('Saving meta file to %s ...'%path_to_lmdb_meta_file)
    meta = Meta()
    meta.num_train_examples = num_train_examples
    meta.num_val_exam = num_val_exam
    meta.num_test_examples = num_test_examples
    meta.save(path_to_lmdb_meta_file)

def main(args):

    path_to_train_dir = os.path.join(args.data_dir, 'train')
    path_to_test_dir = os.path.join(args.data_dir, 'test')
    path_to_extra_dir = os.path.join(args.data_dir, 'extra')
    path_to_train_digit_struct_mat_file = os.path.join(path_to_train_dir, 'digitStruct.mat')
    path_to_test_digit_struct_mat_file = os.path.join(path_to_test_dir, 'digitStruct.mat')
    path_to_extra_digit_struct_mat_file = os.path.join(path_to_extra_dir, 'digitStruct.mat')
    
    path_to_train_lmdb_dir = os.path.join(args.data_dir,'train.lmdb')
    path_to_val_lmdb_dir = os.path.join(args.data_dir,'val.lmdb')
    path_to_test_lmdb_dir = os.path.join(args.data_dir,'test.lmdb')
    path_to_lmdb_meta_file = os.path.join(args.data_dir,'lmdb_meta.json')
    
    for path_to_dir in [path_to_train_lmdb_dir,path_to_val_lmdb_dir,path_to_test_lmdb_dir]:
        assert not os.path.exists(path_to_dir), 'LMDB directory %s already exists' % path_to_dir
    
    print('Processing training and validation data ...')
    [num_train_examples, num_val_examples] = convertlmdb([(path_to_train_dir, path_to_train_digit_struct_mat_file),
                                                         (path_to_extra_dir, path_to_extra_digit_struct_mat_file)],
                                                        [ path_to_train_lmdb_dir, path_to_val_lmdb_dir],
                                                        lambda paths: 0 if random.random()>0.1 else 1)
    print('Processing test data...')
    [num_test_examples] = convertlmdb([(path_to_test_dir,path_to_test_digit_struct_mat_file)],
                                      [path_to_test_lmdb_dir],
                                      lambda paths:0)
    create_lmdb_meta_file(num_train_examples, num_val_examples,num_test_examples,path_to_lmdb_meta_file)
    
    print('Done')

if __name__ == '__main__':
    main(parser.parse_args())